import Goods from './components/goods/goods';
import ratings from './components/ratings/ratings';
import seller from './components/seller/seller';
import star from './components/star/star';

const routes = [
    { path: "/", component: Goods },
    { path: "/goods", component: Goods },
    { path: "/ratings", component: ratings },
    { path: "/seller", component: seller },
    { path: "/star", component: star },
];
export default routes;